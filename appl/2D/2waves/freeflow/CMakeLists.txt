dune_symlink_to_source_files(FILES params.input params_double.input)

dune_add_test(NAME test_ff_rans2cni_2waves
              LABELS 2waves freeflow
              SOURCES main.cc
              CMAKE_GUARD HAVE_UMFPACK
              COMMAND ./test_ff_rans2cni_2waves
              CMD_ARGS params.input)
