dune_symlink_to_source_files(FILES params.input params_double.input)

dune_add_test(NAME test_md_darcy2cni_rans2cni_3waves_sinus
              LABELS 3waves multidomain
              SOURCES main.cc
              CMAKE_GUARD HAVE_UMFPACK
              COMMAND ./test_md_darcy2cni_rans2cni_3waves_sinus
              CMD_ARGS params.input)
