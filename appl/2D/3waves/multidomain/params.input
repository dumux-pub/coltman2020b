[Restart]
DarcyFile = ../initialconditions/darcy_3wave_CC-restart.vtu
RansCCFile = ../initialconditions/rans_3wave_CC-restart.vtu
RansFaceFile = ../initialconditions/rans_3wave_F-restart.vtp

[TimeLoop]
DtInitial =  1e-3 # [s]
MaxTimeStepSize = 7200 # [s] (2 hours)
TEnd = 345600 # [s] (4 days)

[Grid]
Positions0 = -1.5    0.0    0.192  0.204   0.396   0.408   0.600   1.50
Cells0 =              20       48      3      48       3      48     20
Grading0 =         -1.25      1.0    1.0     1.0     1.0     1.0   1.21

Positions1 = 0  0.2  0.28  0.3   0.8
Cells1 =         20    20    5    20
Grading1 =    -1.10  1.00  1.0  1.15

StartDarcy = 0.0 0.2
StartFormOne = 0.000
EndFormOne = 0.192
StartFormTwo = 0.204
EndFormTwo = 0.396
StartFormThree = 0.408
EndFormThree = 0.600
EndDarcy = 0.6 0.2
Amplitude = 0.08
CurveBuffer = 0.005

[RANS.Problem]
Name = rans
InletVelocity = 1. # [m/s]
InletPressure = 1e5 # [Pa]
InletMassFrac = 0.008 # [-]
Temperature = 298.15 # [K]

[Darcy.Problem]
Name = darcy
Pressure = 1e5
InitialSaturation = 0.95 # initial Sw
InitialSaturationAbove = 0.3
Temperature = 298.15 # [K]
InitPhasePresence = 3 # bothPhases

[Darcy.SpatialParams]
Porosity = 0.41
Permeability = 1e-10
AlphaBeaversJoseph = 1.0
Swr = 0.005
Snr = 0.01
VgAlpha = 1.5e-4
VgN = 15

[Problem]
EnableGravity = true
InterfaceDiffusionCoefficientAvg = FreeFlowOnly # Harmonic

[RANS.RANS]
UseStoredEddyViscosity = false

[Vtk]
AddVelocity = true
WriteFaceData = true
OutputName = 3waves_md_single

[Component]
SolidDensity = 2700
SolidThermalConductivity = 2.8
SolidHeatCapacity = 790

[Flux]
TvdApproach = Hou # 1: Uniform TVD, 2: Li's approach, 3: Hou's approach
DifferencingScheme = Vanleer # Vanleer , Vanalbada, Minmod, Superbee, Umist, Mclimiter, Wahyd

[MatrixConverter]
DeletePatternEntriesBelowAbsThreshold = 1e-25

[Newton]
MaxSteps = 10
MaxRelativeShift = 1e-6

[Assembly]
NumericDifferenceMethod = 0
NumericDifference.BaseEpsilon = 1e-8
