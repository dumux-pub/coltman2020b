add_subdirectory(testlump)
dune_symlink_to_source_files(FILES params_2waves.input params_3waves.input)

dune_add_test(NAME test_3d_full_channel_3waves
              LABELS 3D
              SOURCES main_3waves.cc
              CMAKE_GUARD HAVE_UMFPACK
              COMMAND ./test_3d_full_channel_3waves
              CMD_ARGS params_3waves.input)

dune_add_test(NAME test_3d_full_channel_2waves
              LABELS 3D
              SOURCES main_2waves.cc
              CMAKE_GUARD HAVE_UMFPACK
              COMMAND ./test_3d_full_channel_2waves
              CMD_ARGS params_2waves.input)
