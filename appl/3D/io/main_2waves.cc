// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the coupled RANS/Darcy problem
 */
#include <config.h>

#include <ctime>
#include <iostream>
#include <fstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/common/fvector.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/io/file/gmshwriter.hh>
#include <dune/grid/yaspgrid.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/geometry/diameter.hh>
#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/staggeredvtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/grid/gridmanager_sub.hh>

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // Initialize MPI, finalize is done automatically on exit.
    Dune::MPIHelper::instance(argc, argv);

    // First read parameters from input file.
    Dumux::Parameters::init(argc, argv);
    const std::string outputName = getParam<std::string>("Vtk.OutputName");

    // The hostgrid
    constexpr int dim = 3;
    using GlobalPosition = Dune::FieldVector<double, dim>;
    using AxisPosition = Dune::FieldVector<double, dim-1>;

    Dune::Timer timer;
    using HostGrid = Dune::YaspGrid<dim, Dune::TensorProductCoordinates<double, dim> >;
    using SubGrid = Dune::SubGrid<dim, HostGrid>;
    using HostGridManager = GridManager<HostGrid>;
    HostGridManager hostGridManager;
    hostGridManager.init();
    auto& hostGrid = hostGridManager.grid();

    struct Params
    {
        GlobalPosition startTankZone = getParam<GlobalPosition>("Grid.StartTankZone");
        GlobalPosition endTankZone = getParam<GlobalPosition>("Grid.EndTankZone");

        double outerCorridorWidth = getParam<double>("Grid.OuterCorridorWidth");
        double extensionSlopeWidth = getParam<double>("Grid.ExtensionSlopeWidth");
        double extensionFlatWidth = getParam<double>("Grid.ExtensionFlatWidth");
        double tankWidth = getParam<double>("Grid.TankWidth");

        double hillLength = getParam<double>("Grid.HillLength");
        double hillHeight = getParam<double>("Grid.HillHeight");
        double startHillOne = getParam<double>("Grid.StartHillOne");
        double startHillTwo = getParam<double>("Grid.StartHillTwo");
    };
    Params params;

    // The subgrid
    auto ransSelector = [&params](const auto& element)
    {
        double eps = 1e-12;
        const GlobalPosition globalPos = element.geometry().center();

        // Within the tank zone
        const bool inTankZone = (globalPos[0] < params.endTankZone[0] + eps) && (globalPos[0] > params.startTankZone[0] - eps)
                             && (globalPos[1] < params.endTankZone[1] + eps) && (globalPos[1] > params.startTankZone[1] - eps)
                             && (globalPos[2] < params.endTankZone[2] + eps) && (globalPos[2] > params.startTankZone[2] - eps);

        // Tank zones
        const bool inOuterCorridor = ((globalPos[2] < (params.startTankZone[2] + params.outerCorridorWidth + eps))
                                   || (globalPos[2] > (params.endTankZone[2] - params.outerCorridorWidth - eps)));
        const bool inExtensionSlopeLeft = ( (globalPos[2] < params.startTankZone[2] + params.outerCorridorWidth + params.extensionSlopeWidth + eps)
                                         && (globalPos[2] > params.startTankZone[2] + params.outerCorridorWidth - eps));
        const bool inExtensionSlopeRight = ( (globalPos[2] > params.endTankZone[2] - params.outerCorridorWidth - params.extensionSlopeWidth - eps)
                                          && (globalPos[2] < params.endTankZone[2] - params.outerCorridorWidth + eps));
        const bool inCenterHill = ( (globalPos[2] < params.endTankZone[2] - params.outerCorridorWidth - params.extensionSlopeWidth + eps)
                                 && (globalPos[2] > params.startTankZone[2] + params.outerCorridorWidth + params.extensionSlopeWidth - eps) );

        // Hill Zones
        const bool inHillOne = ( (globalPos[0] > params.startHillOne - eps) && (globalPos[0] < params.startHillOne + params.hillLength + eps) );
        const bool inHillTwo = ( (globalPos[0] > params.startHillTwo - eps) && (globalPos[0] < params.startHillTwo + params.hillLength + eps) );

        // Hill Shapes
        const double frequency = params.hillLength / 2.0;
        const double amplitude = params.hillHeight / 2.0;
        const double verticalOffset = params.startTankZone[1] + amplitude;

        const double horizOffsetOne = params.startHillOne + frequency;
        const double argOne = std::cos((globalPos[0] - horizOffsetOne) * M_PI / frequency);
        const bool aboveHillCenterOne = globalPos[1] > ((amplitude * argOne) + verticalOffset + eps);

        const double horizOffsetTwo = params.startHillTwo + frequency;
        const double argTwo = std::cos((globalPos[0] - horizOffsetTwo) * M_PI / frequency);
        const bool aboveHillCenterTwo = globalPos[1] > ((amplitude * argTwo) + verticalOffset + eps);

        // Hill Extension Slope Shapes
        const AxisPosition pointAxis = {globalPos[0], globalPos[2]};
        const double leftSlopePeak = params.startTankZone[2] + params.outerCorridorWidth + params.extensionSlopeWidth;
        const double rightSlopePeak = params.endTankZone[2] - params.outerCorridorWidth - params.extensionSlopeWidth;
            // Left Side Hill Extensions

            const AxisPosition slopeLeftOneCenterAxis = {params.startHillOne + frequency, leftSlopePeak};
            const double rLeftOne = (pointAxis - slopeLeftOneCenterAxis).two_norm();
            const bool inSlopeLeftOne = rLeftOne < (frequency + eps);
            const bool aboveExtSlopeLeftOne = globalPos[1] > ( amplitude * std::cos(rLeftOne * M_PI / (frequency)) + verticalOffset + eps);

            const AxisPosition slopeLeftTwoCenterAxis = {params.startHillTwo + frequency, leftSlopePeak};
            const double rLeftTwo = (pointAxis - slopeLeftTwoCenterAxis).two_norm();
            const bool inSlopeLeftTwo = rLeftTwo < (frequency + eps);
            const bool aboveExtSlopeLeftTwo = globalPos[1] > ( amplitude * std::cos(rLeftTwo * M_PI / (frequency)) + verticalOffset + eps);

            // Right Side Hill Extensions
            const AxisPosition slopeRightOneCenterAxis = {params.startHillOne + frequency, rightSlopePeak};
            const double rRightOne = (pointAxis - slopeRightOneCenterAxis).two_norm();
            const bool inSlopeRightOne = rRightOne < (frequency + eps);
            const bool aboveExtSlopeRightOne = globalPos[1] > ( amplitude * std::cos(rRightOne * M_PI / (frequency)) + verticalOffset + eps);

            const AxisPosition slopeRightTwoCenterAxis = {params.startHillTwo + frequency, rightSlopePeak};
            const double rRightTwo = (pointAxis - slopeRightTwoCenterAxis).two_norm();
            const bool inSlopeRightTwo = rRightTwo < (frequency + eps);
            const bool aboveExtSlopeRightTwo = globalPos[1] > ( amplitude * std::cos(rRightTwo * M_PI / (frequency)) + verticalOffset + eps);

        if (inTankZone)
        {
            if (inOuterCorridor)
                return true;
            else if (inHillOne)
            {
                if (inCenterHill)
                    return aboveHillCenterOne;
                else if (inExtensionSlopeLeft && inSlopeLeftOne)
                    return aboveExtSlopeLeftOne;
                else if (inExtensionSlopeRight && inSlopeRightOne)
                    return aboveExtSlopeRightOne;
                else
                    return true;
            }
            else if (inHillTwo)
            {
                if (inCenterHill)
                    return aboveHillCenterTwo;
                else if (inExtensionSlopeLeft && inSlopeLeftTwo)
                    return aboveExtSlopeLeftTwo;
                else if (inExtensionSlopeRight && inSlopeRightTwo)
                    return aboveExtSlopeRightTwo;
                else
                    return true;
            }
            else
                return true;
        }
        else
            return true;
    };

    Dumux::GridManager<SubGrid> ransSubGridManager;
    ransSubGridManager.init(hostGrid, ransSelector, "RANS");

    Dune::VTKWriter<typename SubGrid::LeafGridView> vtkWriter(ransSubGridManager.grid().leafGridView());
    vtkWriter.write(outputName);

    Dune::GmshWriter<typename SubGrid::LeafGridView> gmshWriter(ransSubGridManager.grid().leafGridView());
    gmshWriter.setPrecision(10);
    gmshWriter.write(outputName + ".msh");
        std::cout << "Msh file written as output \n";

    return 0;
}
///////////////////////////////////////
//////// Error handler ////////////////
///////////////////////////////////////
catch (const Dumux::ParameterException& e) {
    std::cerr << e << ". Abort!\n";
    return 1;
}
catch (const Dune::Exception& e) {
    std::cerr << "Dune reported error: " << e << std::endl;
    return 3;
}
catch (...) {
    std::cerr << "Unknown exception thrown!\n";
    return 4;
}
