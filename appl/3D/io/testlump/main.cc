// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the coupled RANS/Darcy problem
 */
#include <config.h>

#include <ctime>
#include <iostream>
#include <fstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/common/fvector.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/grid/yaspgrid.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/geometry/diameter.hh>
#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/staggeredvtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/grid/gridmanager_sub.hh>

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // Initialize MPI, finalize is done automatically on exit.
    Dune::MPIHelper::instance(argc, argv);

    // First read parameters from input file.
    Dumux::Parameters::init(argc, argv);

    // The hostgrid
    constexpr int dim = 3;
    using GlobalPosition = Dune::FieldVector<double, dim>;
    using AxisPosition = Dune::FieldVector<double, dim-1>;

    Dune::Timer timer;
    using HostGrid = Dune::YaspGrid<dim, Dune::TensorProductCoordinates<double, dim> >;
    using SubGrid = Dune::SubGrid<dim, HostGrid>;
    using HostGridManager = GridManager<HostGrid>;
    HostGridManager hostGridManager;
    hostGridManager.init();
    auto& hostGrid = hostGridManager.grid();

    struct Params
    {
        GlobalPosition lumpCenter = getParam<GlobalPosition>("Grid.LumpCenter");
        double lumpRadius = getParam<double>("Grid.LumpRadius");
        double lumpHeight = getParam<double>("Grid.LumpHeight");
    };
    Params params;

    // The subgrid
    auto ransSelector = [&params](const auto& element)
    {
        double eps = 1e-12;
        const GlobalPosition globalPos = element.geometry().center();
        const AxisPosition lumpCenterAxis = {params.lumpCenter[0], params.lumpCenter[2]};
        const AxisPosition pointAxis = {globalPos[0], globalPos[2]};

        // try and convert to cylndrical coodinates. origin at lumpCenterAxis
        const double r = (pointAxis - lumpCenterAxis).two_norm();

        const bool inObjectCylinder = r < (params.lumpRadius + eps);
        const bool belowCurve = globalPos[1] < ( params.lumpHeight/2.0 * std::cos(r * M_PI / (params.lumpRadius)) + params.lumpHeight/2.0 + eps);

        if (inObjectCylinder)
            return !belowCurve;
        else
            return true;
    };

    Dumux::GridManager<SubGrid> ransSubGridManager;
    ransSubGridManager.init(hostGrid, ransSelector, "RANS");
    Dune::VTKWriter<typename SubGrid::LeafGridView> vtkWriter(ransSubGridManager.grid().leafGridView());
    vtkWriter.write("lumpTest");
    std::cout << "Constructing a yasp subgrid took "  << timer.elapsed() << " seconds.\n";

    return 0;
}
///////////////////////////////////////
//////// Error handler ////////////////
///////////////////////////////////////
catch (const Dumux::ParameterException& e) {
    std::cerr << e << ". Abort!\n";
    return 1;
}
catch (const Dune::Exception& e) {
    std::cerr << "Dune reported error: " << e << std::endl;
    return 3;
}
catch (...) {
    std::cerr << "Unknown exception thrown!\n";
    return 4;
}
