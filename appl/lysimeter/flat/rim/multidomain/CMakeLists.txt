dune_symlink_to_source_files(FILES params.input)

dune_add_test(NAME test_md_darcy2cni_rans2cni_flat_rim
              LABELS flat rim multidomain
              SOURCES main.cc
              CMAKE_GUARD HAVE_UMFPACK
              COMMAND ./test_md_darcy2cni_rans2cni_flat_rim
              CMD_ARGS params.input)
