dune_symlink_to_source_files(FILES params.input)

dune_add_test(NAME test_pm_darcy2cni_flat_rim
              LABELS rim flat porousmediumflow
              SOURCES main.cc
              CMAKE_GUARD HAVE_UMFPACK
              COMMAND ./test_pm_darcy2cni_flat_rim
              CMD_ARGS params.input)
