echo " "
echo " "
echo " "
echo " "
echo "*********************************************************************************************"
echo "(1/3) Checking all prerequistes. (git cmake gcc g++ wget pkg-config gnuplot umfpack)"
echo "*********************************************************************************************"

# check some prerequistes
for PRGRM in git cmake gcc g++ wget pkg-config gnuplot; do
    if ! [ -x "$(command -v $PRGRM)" ]; then
        echo "Error: $PRGRM is not installed." >&2
        exit 1
    fi
done

# check some library prerequistes
for LIBRARY in libumfpack; do
    if ! [ "$(/sbin/ldconfig -p | grep $LIBRARY)" ]; then
        echo "Error: $LIBRARY is not installed." >&2
        exit 1
    fi
done

currentver="$(gcc -dumpversion)"
requiredver="4.9.0"
if [ "$(printf '%s\n' "$requiredver" "$currentver" | sort -V | head -n1)" != "$requiredver" ]; then
    echo "gcc greater than or equal to $requiredver is required!" >&2
    exit 1
fi
if [ $? -ne 0 ]; then
    echo "*********************************************************************************************"
    echo "(1/3) An error occured."
    echo "*********************************************************************************************"
    exit $?
else
    echo "*********************************************************************************************"
    echo "(1/3) All prerequistes found."
    echo "*********************************************************************************************"
fi

echo " "
echo " "
echo "*********************************************************************************************"
echo "(2/3) Cloning repositories into a containing folder. Make sure to be connected to the internet."
echo "*********************************************************************************************"

# make a new folder containing everything
mkdir $(pwd)/dumuxColtman2020b
cd dumuxColtman2020b/

# dumux-Pub Module
if [ ! -d "coltman2020b" ]; then
    git clone https://git.iws.uni-stuttgart.de/dumux-pub/coltman2020b.git
    cd coltman2020b
    git checkout master
    cd ..
else
    echo "Skip cloning dumux because the folder already exists."
    cd coltman2020b
    git checkout master
    cd ..
fi

# dune-common
if [ ! -d "dune-common" ]; then
    git clone https://gitlab.dune-project.org/core/dune-common.git
    cd dune-common
    git checkout master
    cd ..
else
    echo "Skip cloning dune-common because the folder already exists."
    cd dune-common
    git checkout master
    cd ..
fi

# dune-geometry
if [ ! -d "dune-geometry" ]; then
    git clone https://gitlab.dune-project.org/core/dune-geometry.git
    cd dune-geometry
    git checkout master
    cd ..
else
    echo "Skip cloning dune-geometry because the folder already exists."
    cd dune-geometry
    git checkout master
    cd ..
fi

# dune-grid
if [ ! -d "dune-grid" ]; then
    git clone https://gitlab.dune-project.org/core/dune-grid.git
    cd dune-grid
    git checkout master
    cd ..
else
    echo "Skip cloning dune-grid because the folder already exists."
    cd dune-grid
    git checkout master
    cd ..
fi

# dune-localfunctions
if [ ! -d "dune-localfunctions" ]; then
    git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
    cd dune-localfunctions
    git checkout master
    cd ..
else
    echo "Skip cloning dune-localfunctions because the folder already exists."
    cd dune-localfunctions
    git checkout master
    cd ..
fi

# dune-istl
if [ ! -d "dune-istl" ]; then
    git clone https://gitlab.dune-project.org/core/dune-istl.git
    cd dune-istl
    git checkout master
    cd ..
else
    echo "Skip cloning dune-istl because the folder already exists."
    cd dune-istl
    git checkout master
    cd ..
fi

# dune-subgrid
if [ ! -d "dune-subgrid" ]; then
    git clone https://git.imp.fu-berlin.de/agnumpde/dune-subgrid.git
    cd dune-subgrid
    git checkout master
    cd ..
    git apply < ./coltman2020b/fix_subgrid.patch
else
    echo "Skip cloning dune-subgrid because the folder already exists."
    cd dune-subgrid
    git checkout master
    cd ..
    git apply < ./coltman2020b/fix_subgrid.patch
fi

# dumux
if [ ! -d "dumux" ]; then
    git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
    cd dumux
    git checkout master
    cd ..
else
    echo "Skip cloning dumux because the folder already exists."
    cd dumux
    git checkout master
    cd ..
fi

if [ $? -ne 0 ]; then
    echo "*********************************************************************************************"
    echo "(2/3) Failed to clone the repositories. Look for individual error messages."
    echo "*********************************************************************************************"
    exit $?
else
    echo "*********************************************************************************************"
    echo "(2/3) All repositories have been cloned into a containing folder."
    echo "*********************************************************************************************"
fi

echo " "
echo " "
echo "*********************************************************************************************"
echo "(3/3) Configure dune modules and dumux. Build the dune libaries. This may take several minutes."
echo "*********************************************************************************************"

# run build
./dune-common/bin/dunecontrol --opts=./dumux/cmake.opts all

if [ $? -ne 0 ]; then
    echo "*********************************************************************************************"
    echo "(3/3) Failed to build the dune libaries."
    echo "*********************************************************************************************"
    exit $?
else
    echo "*********************************************************************************************"
    echo "(3/3) Succesfully configured and built dune and dumux."
    echo "*********************************************************************************************"
fi
